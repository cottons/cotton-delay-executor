'use strict';

var delayExecutor = function (delay) {

    var self = {}, timeoutId;

    self.execute = function (action) {
        clearTimeout(timeoutId);
        timeoutId = setTimeout(action, delay);
    };

    self.cancel = function () {
        clearTimeout(timeoutId);
    };

    return self;
};
